# LaTeX Resume

This repository houses the LaTeX source for my resume, as well as an automatic
deployment system that builds the ```.tex``` into a ```.pdf``` file, and then 
transfers it to my live website ([HiveRacing.cc](http://HiveRacing.cc)) 


### Notes
Feel absolutely free to take the ```.tex``` file and use it as a template for 
your resume! Finding a decent template to start out with when transferring my 
resume to a LaTeX format was exceptionally hard, so hopefully someone can 
make some use out of this!

